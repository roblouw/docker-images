package com.example.demo


import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@Suppress("unused")
class HelloController {

    @GetMapping("/")
    fun henlo() = "henlo"
}
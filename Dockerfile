FROM amazoncorretto:11-alpine

RUN addgroup -S gradle && adduser -S gradle -G gradle
USER appuser